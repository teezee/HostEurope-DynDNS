**FAIR LICENSE**

Copyright (c) 2016-2017 thomas.zink _at_ uni-konstanz _dot_ de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

# HostEurope-DynDNS

Update HostEurope DNS entries with dynamic IP addresses and adjust the TTL.

## Requirements

Requires `wget`, `curl`, and `miniupnpc`.

### Debian and derivates

	apt update
	apt install -y wget curl miniupnpc

### Alpine

First enable community repo and then install

	sed -ri 's/(http)(:\/\/.+v\d\.\d\/main)/\1s\2/g' /etc/apk/repositories
	sed -ri 's/#(http)(:\/\/.+)(v\d\.\d\/community)/\1s\2\3/g' /etc/apk/repositories
	apk update
	apk add wget curl miniupnpc

## Configuration

### HostEurope

Prior to using this script, the domain and the host must be configured in KIS.

1. Login to KIS and visit the Domain Administration page: [https://kis.hosteurope.de/administration/domainservices/index.php?menu=2&mode=autodns](https://kis.hosteurope.de/administration/domainservices/index.php?menu=2&mode=autodns)
2. Find the Domain you want to update (e.g. `example.com`) and click on `edit`
3. If not done already, add host (e.g. `host.example.com`), else goto 4.
4. Find the `hostid` of the host
	* View Page Source
	* Search for host (e.g. `host.example.com`)
	* Look for the following input filed:
		`<input type="hidden" name="hostid" value="XXXXXXXXX" />`
	* Copy the `value`, which is the host's `hostid`

### iptables

If you run `iptables` on the host you need to add a rule to allow UPNP from your UPNP device (most likely your internet access router).

	# iptables allow upnp from router
	-A INPUT -p udp -s $ROUTER_IP --sport 1900 -j ACCEPT

## Usage

The script needs 5 variables, which can be passed through the command line or a configuration file.

	USERID="your KIS user"
	PASSWD="securePassword"
	DOMAIN="example.com"
	HOSTID="XXXXXXXXX"
	TTL=ttl_value

Possible TTLs:

* 86400 (1 day)
* 300 (5 mins)
* 900 (15 mins)
* 1800 (30 mins)
* 3600 (1 hour)
* 14400 (4 hours)
* 43200 (12 hours)

### Example

Run the script using a configfile

	hosteurope-dyndns

Run the script with arguments

	hosteurope-dyndns USERID PASSWD DOMAIN HOSTID TTL

## References

An alternative, which was also an inspiration is [https://github.com/AndreasPrang/HostEurope-dynDNS](https://github.com/AndreasPrang/HostEurope-dynDNS). The author also provides a dockerfile to run his script in a container. If you run docker, this might be a better alternative for you.


